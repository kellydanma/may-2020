package day03

func canConstruct(ransomNote string, magazine string) bool {
	letters := map[rune]int{}
	for _, r := range magazine {
		letters[r]++
	}
	for _, r := range ransomNote {
		if letters[r] > 0 {
			letters[r]--
			continue
		}
		return false
	}
	return true
}
