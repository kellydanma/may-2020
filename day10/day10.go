package day10

func findJudge(N int, trust [][]int) int {
	data := make([]int, N+1)
	for _, t := range trust {
		data[t[0]], data[t[1]] = data[t[0]]-1, data[t[1]]+1
	}
	for i := 1; i <= N; i++ {
		if data[i] == N-1 {
			return i
		}
	}
	return -1
}
