# 31-day LeetCode Challenge

My solutions to the [31-day Challenge](https://leetcode.com/discuss/general-discussion/595334/may-leetcoding-challenge) for May 2020.

## Challenges

1. [Day 1 - First Bad Version](./day01/day01.md)
2. [Day 2 - Jewels & Stones](./day02/day02.md)
3. [Day 3 - Ransom Note](./day03/day03.md)
4. [Day 4 - Number Complement](./day04/day04.md)
5. [Day 5 - First Unique Character in a String](./day05/day05.md)
6. [Day 6 - Majority Element](./day06/day06.md)
7. [Day 7 - Cousins in Binary Tree](./day07/day07.md)
8. [Day 8 - Check If It Is a Straight Line](./day08/day08.md)
9. [Day 9 - Valid Perfect Square](./day09/day09.md)
10. [Day 10 - Find the Town Judge](./day10/day10.md)
11. [Day 11 - Flood Fill](./day11/day11.md)
12. [Day 12 - Single Element in a Sorted Array](./day12/day12.md)
13. [Day 13 - Remove K Digits](./day13/day13.md)
14. [Day 14 - Implement Trie (Prefix Tree)](./day14/day14.md)
15. [Day 15 - Maximum Sum Circular Subarray](./day15/day15.md)
16. [Day 16 - Odd Even Linked List](./day16/day16.md)
17. [Day 17 - Find All Anagrams in a String](./day17/day17.md)
18. [Day 18 - Permutation in String](./day18/day18.md)
19. [Day 19 - Online Stock Spanner](./day19/day19.md)
20. [Day 20 - Kth Smallest Element in a BST](./day20/day20.md)
21. [Day 21 - Count Square Submatrices with All Ones](./day21/day21.md)
22. [Day 22 - Sort Characters By Frequency](./day22/day22.md)
