package day11

func floodFill(image [][]int, sr int, sc int, newColour int) [][]int {
	colour := image[sr][sc]
	if colour != newColour {
		dfs(image, sr, sc, colour, newColour)
	}
	return image
}

func dfs(image [][]int, r int, c int, col int, newCol int) {
	if image[r][c] == col {
		image[r][c] = newCol
		if r >= 1 {
			dfs(image, r-1, c, col, newCol)
		}
		if c >= 1 {
			dfs(image, r, c-1, col, newCol)
		}
		if r+1 < len(image) {
			dfs(image, r+1, c, col, newCol)
		}
		if c+1 < len(image[0]) {
			dfs(image, r, c+1, col, newCol)
		}
	}
}
