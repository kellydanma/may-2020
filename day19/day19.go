package day19

type StockSpanner struct {
	Prices []int
	Weight []int
}

func Constructor() StockSpanner {
	return StockSpanner{
		make([]int, 0, 16),
		make([]int, 0, 16),
	}
}

func (this *StockSpanner) Next(price int) int {
	w, i := 1, len(this.Prices)-1
	for i >= 0 && this.Prices[i] <= price {
		w += this.Weight[i]
		i--
	}
	i++
	if i >= 0 {
		this.Prices = this.Prices[:i]
		this.Weight = this.Weight[:i]
	} else {
		// this condition won't be hit
		this.Prices = this.Prices[:0]
		this.Weight = this.Weight[:0]
	}
	this.Prices = append(this.Prices, price)
	this.Weight = append(this.Weight, w)
	return w
}
