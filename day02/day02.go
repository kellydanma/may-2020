package day02

func numJewelsInStones(J string, S string) int {
	isJewel := map[rune]bool{}
	for _, j := range J {
		isJewel[j] = true
	}
	jewels := 0
	for _, s := range S {
		if isJewel[s] {
			jewels++
		}
	}
	return jewels
}
