# First Unique Character in a String

Given a string, find the first non-repeating character in it and return it's index. If it doesn't exist, return `-1`.

**Note**: You may assume the string contain only lowercase letters.

## Examples

### Example 1

```
s = "leetcode"
return 0.
```

### Example 2

```
s = "loveleetcode",
return 2.
```

## Solution

See the solution [here](./day05.go).
