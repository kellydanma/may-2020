package day05

func firstUniqChar(s string) int {
	unique := map[rune]int{}
	for _, r := range s {
		unique[r]++
	}
	for i, r := range s {
		if unique[r] == 1 {
			return i
		}
	}
	return -1
}
