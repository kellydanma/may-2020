package day22

func frequencySort(s string) string {
	cache := make(map[rune]int)
	for _, r := range s {
		cache[r]++
	}
	buckets := make([][]rune, len(s)+1)
	for r, f := range cache {
		buckets[f] = append(buckets[f], r)
	}
	result := make([]rune, 0, len(s))
	for i := len(buckets) - 1; i >= 0; i-- {
		bucket := buckets[i]
		if len(bucket) == 0 {
			continue
		}
		for j := 0; j < len(bucket); j++ {
			result = append(result, repeat([]rune{bucket[j]}, i)...)
		}
	}
	return string(result)
}

func repeat(r []rune, count int) []rune {
	if count == 0 {
		return []rune{}
	}
	nr := make([]rune, len(r)*count)
	rp := copy(nr, r)
	for rp < len(nr) {
		copy(nr[rp:], nr[:rp])
		rp *= 2
	}
	return nr
}
