package day14

type Trie struct {
	root map[rune]interface{}
}

func Constructor() Trie {
	return Trie{root: map[rune]interface{}{}}
}

// Inserts adds a word to the Trie.
func (t *Trie) Insert(word string) {
	node := t.root
	for _, c := range word {
		if node[c-'a'] == nil {
			node[c-'a'] = map[rune]interface{}{}
		}
		node = node[c-'a'].(map[rune]interface{})
	}
	node[26] = struct{}{}
}

// Search returns true if the word is in the Trie.
func (t *Trie) Search(word string) bool {
	node := t.root
	for _, c := range word {
		if node[c-'a'] == nil {
			return false
		}
		node = node[c-'a'].(map[rune]interface{})
	}
	return node[26] != nil
}

// StartsWith returns true if any word in the Trie starts with the given prefix.
func (t *Trie) StartsWith(prefix string) bool {
	node := t.root
	for _, c := range prefix {
		if node[c-'a'] == nil {
			return false
		}
		node = node[c-'a'].(map[rune]interface{})
	}
	return true
}
