package day08

func checkStraightLine(coordinates [][]int) bool {
	x0, y0 := coordinates[0][0], coordinates[0][1]
	x1, y1 := coordinates[1][0], coordinates[1][1]
	dx, dy := x1-x0, y1-y0
	xp := dx*y0 - dy*x0
	coordinates = coordinates[2:]
	for i := range coordinates {
		if dx*coordinates[i][1]-dy*coordinates[i][0] != xp {
			return false
		}
	}
	return true
}
