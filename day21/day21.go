package day21

func countSquares(matrix [][]int) int {
	res := 0
	for i := range matrix {
		for j := range matrix[i] {
			if matrix[i][j] > 0 && i > 0 && j > 0 {
				matrix[i][j] = min(matrix[i-1][j-1], min(matrix[i-1][j], matrix[i][j-1])) + 1
			}
			res += matrix[i][j]
		}
	}
	return res
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
