package day15

func maxSubarraySumCircular(A []int) int {
	l, ans, cur := len(A), A[0], A[0]
	for i := 1; i < l; i++ {
		cur = A[i] + max(cur, 0)
		ans = max(ans, cur)
	}

	// ans is the answer for 1-interval subarrays.
	// Now, let's consider all 2-interval subarrays.
	// For each i, we want to know
	// the maximum of sum(A[j:]) with j >= i+2
	// rightsums[i] = A[i] + A[i+1] + ... + A[N-1]

	rightSums := make([]int, l)
	rightSums[l-1] = A[l-1]
	for i := l - 2; i >= 0; i-- {
		rightSums[i] = rightSums[i+1] + A[i]
	}

	// maxright[i] = max_{j >= i} rightsums[j]
	maxRight := make([]int, l)
	maxRight[l-1] = A[l-1]
	for i := l - 2; i >= 0; i-- {
		maxRight[i] = max(maxRight[i+1], rightSums[i])
	}

	leftSum := 0
	for i := 0; i < l-2; i++ {
		leftSum += A[i]
		ans = max(ans, leftSum+maxRight[i+2])
	}

	return ans
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
