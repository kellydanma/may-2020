package day12

func singleNonDuplicate(nums []int) int {
	s := nums[0]
	nums = nums[1:]
	for _, n := range nums {
		s ^= n
	}
	return s
}
