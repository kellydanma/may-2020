package day06

func majorityElement(nums []int) int {
	store, maj := map[int]int{}, len(nums)/2
	for _, n := range nums {
		store[n]++
		if freq := store[n]; freq > maj {
			return n
		}
	}
	return 0
}
