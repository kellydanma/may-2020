package day07

// TreeNode is a binary tree node.
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func isCousins(root *TreeNode, x int, y int) bool {
	_, xd, xp := findNode(root, -1, x, 0)
	_, yd, yp := findNode(root, -1, y, 0)
	if xd == yd && xp != yp {
		return true
	}
	return false
}

func findNode(n *TreeNode, parentV, targetV, depth int) (bool, int, int) {
	if n == nil {
		return false, 0, 0
	}
	if n.Val == targetV {
		return true, depth, parentV
	}
	if f, d, p := findNode(n.Left, n.Val, targetV, depth+1); f {
		return f, d, p
	}
	if f, d, p := findNode(n.Right, n.Val, targetV, depth+1); f {
		return f, d, p
	}
	return false, 0, 0
}
