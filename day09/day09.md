# Valid Perfect Square

Given a positive integer `num`, write a function which returns `true` if num is a perfect square else `false`.

**Note**: Do not use any built-in library function such as `sqrt`.

## Examples

### Example 1

```
Input: 16
Output: true
```

### Example 2

```
Input: 14
Output: false
```

## Solution

See the solution [here](./day09.go).
